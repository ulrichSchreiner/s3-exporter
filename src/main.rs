#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate prometheus;
extern crate s3;
use actix_web::{get, rt::time::timeout, web, App, HttpResponse, HttpServer, Responder};
use clap::{self, value_t};
use futures::{stream, StreamExt};
use prometheus::{Encoder, HistogramVec, TextEncoder};
use s3::bucket::Bucket;
use s3::creds::Credentials;
use s3::region::Region;
use s3::S3Error;
use serde::{Deserialize, Serialize};
use std::{fmt, str, time::Duration};
#[macro_use]
extern crate slog;
extern crate slog_async;
extern crate slog_term;
use slog::{Drain, Logger};

lazy_static! {
    static ref S3_LS_HISTOGRAM: HistogramVec = register_histogram_vec!(
        "s3_bucket_ls_duration",
        "Duration for the list of root elements",
        &["s3bucket", "endpoint"]
    )
    .unwrap();
}

const VERSION: &'static str = env!("CARGO_PKG_VERSION");

#[derive(Debug, Clone, Serialize, Deserialize)]
struct S3Storage {
    name: String,
    region: Option<String>,
    access_key: String,
    secret_key: String,
    bucket: String,
    endpoint: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct Config {
    storages: Vec<S3Storage>,
}

impl fmt::Display for Config {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // only dump debug formatted self
        write!(f, "{:?}", self)
    }
}
#[derive(Debug, Clone)]
struct MetricsApp {
    config: Config,
    concurrent: u32,
    timeout: u32,
    log: Logger,
}

async fn gather_bucket(log: &Logger, tmout: u32, backend: S3Storage) -> Result<(), S3Error> {
    info!(log, "execute";  "backend" => &backend.name, "endpoint" => &backend.endpoint);
    let endpoint = backend.endpoint.clone();
    let region = Region::Custom {
        region: backend.region.unwrap_or("".into()),
        endpoint: backend.endpoint.into(),
    };
    let credentials = Credentials::new(
        Some(&backend.access_key),
        Some(&backend.secret_key),
        None,
        None,
        None,
    )?;

    let mut bucket = Bucket::new_with_path_style(&backend.bucket, region, credentials)?;
    bucket.add_header("User-Agent", "s3exporter");

    // List out contents of directory

    let timer = S3_LS_HISTOGRAM
        .with_label_values(&[&backend.bucket, &endpoint])
        .start_timer();
    let f = bucket.list("".to_string(), Some("/".to_string()));
    match timeout(Duration::from_secs(tmout as u64), f).await {
        Err(_e) => {
            return Err(S3Error::from(
                format!("call timed out: {}", endpoint).as_str(),
            ));
        }
        Ok(r) => match r {
            Err(e) => return Err(e),
            _ => (),
        },
    }
    timer.observe_duration();

    Ok(())
}

async fn gather(app: MetricsApp) {
    let tmout = app.timeout;
    let log = &app.log;

    stream::iter(
        app.config
            .storages
            .into_iter()
            .map(|b| gather_bucket(log, tmout, b)),
    )
    .for_each_concurrent(app.concurrent as usize, |r| async move {
        match r.await {
            Err(e) => error!(log, "Error: {:?}", e),
            _ => (),
        }
    })
    .await;
}

#[get("/metrics")]
async fn metrics(data: web::Data<MetricsApp>) -> impl Responder {
    gather(data.get_ref().clone()).await;

    let encoder = TextEncoder::new();
    let metric_families = prometheus::gather();
    let mut buffer = vec![];
    encoder.encode(&metric_families, &mut buffer).unwrap();

    HttpResponse::Ok().body(buffer)
}

async fn load_config(pt: &str) -> Result<Config, Box<dyn std::error::Error>> {
    let f = std::fs::File::open(pt)?;
    let c: Config = serde_yaml::from_reader(f)?;

    Ok(c)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let decorator = slog_term::TermDecorator::new().build();
    let drain = slog_term::FullFormat::new(decorator).build().fuse();
    let drain = slog_async::Async::new(drain).build().fuse();

    let log = slog::Logger::root(drain, o!());

    let matches = clap::App::new("S3 metrics exporter")
        .version(VERSION)
        .arg(
            clap::Arg::with_name("config")
                .short("c")
                .long("config")
                .takes_value(true)
                .default_value("/etc/s3-metrics.yaml")
                .help("a configuration file"),
        )
        .arg(
            clap::Arg::with_name("listen")
                .short("l")
                .long("listen")
                .takes_value(true)
                .default_value("127.0.0.1:8080")
                .help("the listen address"),
        )
        .arg(
            clap::Arg::with_name("concurrency")
                .short("r")
                .long("concurrency")
                .takes_value(true)
                .default_value("1")
                .help("the number of concurrent bucket requests"),
        )
        .arg(
            clap::Arg::with_name("timeout-sec")
                .short("t")
                .long("timeout-sec")
                .takes_value(true)
                .default_value("10")
                .help("the maximum timeout for a remote call in seconds"),
        )
        .get_matches();

    let concurrency = value_t!(matches.value_of("concurrency"), u32).unwrap_or(1);
    let myfile = matches.value_of("config").unwrap_or("/etc/s3-metrics.yaml");
    let tmout = value_t!(matches.value_of("timeout-sec"), u32).unwrap_or(1);

    let cfg = match load_config(myfile).await {
        Ok(c) => c,
        Err(e) => panic!("Problem reading the file: {:?}", e),
    };
    let listen = matches.value_of("listen").unwrap();
    info!(&log, "startup"; "listen" => listen, "config" => %&cfg, "concurrency"=>concurrency, "timeout" => tmout);

    HttpServer::new(move || {
        App::new()
            .data(MetricsApp {
                config: cfg.clone(),
                concurrent: concurrency,
                timeout: tmout,
                log: log.clone(),
            })
            .service(metrics)
    })
    .bind(listen)?
    .run()
    .await
}
