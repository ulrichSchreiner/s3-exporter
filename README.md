# S3 exporter

This is a simple metrics exporter for S3 buckets. It is only written to monitor
my buckets hosted by a ceph powered S3, so do not expect it to work with other
S3 implementations.

The program simply executes a `ls /` for every configured bucket and updates a
histogram vector.

To use the metric, you can simply take a query like this one:

~~~
rate(s3_bucket_ls_duration_sum{endpoint=~"$endpoint"}[5m]) / rate(s3_bucket_ls_duration_count{endpoint=~"$endpoint"}[5m])
~~~

## Usage

Create a configuration file with the following contents:

~~~yaml
storages:
  - name: buck1
    access_key: accesskey1
    secret_key: secretkey1
    bucket: my-bucket1
    endpoint: https://my.url
~~~

now start the service:

~~~sh
docker run -it --rm \
    -p 8080:8080 \
    -v /my/path/to/config.yaml:/etc/s3-metrics.yaml:ro \
    registry.gitlab.com/ulrichschreiner/s3-exporter \
    s3-exporter --listen 0.0.0.0:8080
~~~

And now you can grab the metrics with `http://localhost:8080/metrics`.

Current available options:
~~~
S3 metrics exporter 0.1.0

USAGE:
s3-exporter [OPTIONS]

FLAGS:
-h, --help       Prints help information
-V, --version    Prints version information

OPTIONS:
-r, --concurrency <concurrency>    the number of concurrent bucket requests [default: 1]
-c, --config <config>              a configuration file [default: /etc/s3-metrics.yaml]
-l, --listen <listen>              the listen address [default: 127.0.0.1:8080]
-t, --timeout-sec <timeout-sec>    the maximum timeout for a remote call in seconds [default: 10]
~~~

If you have many buckets to test, you should increase the concurrency level to a
higher number. Do not increase too high, because the measurement of the durations on
the client side will be blurred.

You can also decrease the timeout for each request if you safely know that every
response has to happen within a maximum duration.