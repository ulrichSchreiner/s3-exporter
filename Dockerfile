FROM rust:1 as builder
WORKDIR /usr/src/s3-exporter
COPY . .
RUN cargo install --path .

FROM registry.gitlab.com/ulrichschreiner/base/debian:buster-slim
RUN apt -y update && \
    apt -y install ca-certificates curl tzdata && \
    rm -rf /var/lib/apt/lists/*

COPY --from=builder /usr/local/cargo/bin/s3-exporter /usr/local/bin/s3-exporter
RUN useradd --no-create-home --user-group --shell /bin/bash --home-dir /work --uid 1234 exporter
USER exporter
CMD ["s3-exporter"]